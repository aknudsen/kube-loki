{
  manifests(namespace='logging', promtailImageTag='latest', promtailImagePullPolicy='Always',
    lokiImageTag='latest', lokiImagePullPolicy='Always', lokiStorageSize=''):: {
    ['0-' + namespace + '-namespace']: {
      apiVersion: 'v1',
      kind: 'Namespace',
      metadata: {
        name: namespace,
      },
    },
    'promtail-serviceaccount': {
      apiVersion: 'v1',
      kind: 'ServiceAccount',
      metadata: {
        name: 'promtail',
        namespace: namespace,
        labels: {
          app: 'promtail',
        },
      },
    },
    'promtail-clusterrole': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'ClusterRole',
      metadata: {
        name: 'promtail',
        labels: {
          app: 'promtail',
        },
      },
      rules: [
        {
          apiGroups: [''],
          resources: [
            'nodes',
            'nodes/proxy',
            'services',
            'endpoints',
            'pods',
          ],
          verbs: [
            'list',
            'get',
            'watch',
          ],
        },
      ],
    },
    'promtail-clusterrolebinding': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'ClusterRoleBinding',
      metadata: {
        name: 'promtail',
        labels: {
          app: 'promtail',
        },
      },
      subjects: [
        {
          kind: 'ServiceAccount',
          name: 'promtail',
          namespace: namespace,
        },
      ],
      roleRef: {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'ClusterRole',
        name: 'promtail',
      },
    },
    'promtail-role': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'Role',
      metadata: {
        name: 'promtail',
        namespace: namespace,
        labels: {
          app: 'promtail',
        },
      },
      rules: [
        {
          apiGroups: ['extensions'],
          resources: ['podsecuritypolicies'],
          verbs: ['use'],
          resourceNames: ['promtail'],
        },
      ],
    },
    'promtail-rolebinding': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'RoleBinding',
      metadata: {
        name: 'promtail',
        namespace: namespace,
        labels: {
          app: 'promtail',
        },
      },
      roleRef: {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'Role',
        name: 'promtail',
      },
      subjects: [
        {
          kind: 'ServiceAccount',
          name: 'promtail',
        },
      ],
    },
    'promtail-podsecuritypolicy': {
      apiVersion: 'policy/v1beta1',
      kind: 'PodSecurityPolicy',
      metadata: {
        labels: {
          app: 'promtail',
        },
        name: 'promtail',
        namespace: namespace,
      },
      spec: {
        allowPrivilegeEscalation: false,
        fsGroup: {
          rule: 'RunAsAny'
        },
        hostIPC: false,
        hostNetwork: false,
        hostPID: false,
        privileged: false,
        readOnlyRootFilesystem: true,
        requiredDropCapabilities: [
          'ALL'
        ],
        runAsUser: {
          rule: 'RunAsAny'
        },
        seLinux: {
          rule: 'RunAsAny'
        },
        supplementalGroups: {
          rule: 'RunAsAny'
        },
        volumes: [
          'secret',
          'configMap',
          'hostPath'
        ]
      },
    },
    'promtail-daemonset': {
      apiVersion: 'apps/v1',
      kind: 'DaemonSet',
      metadata: {
        labels: {
          app: 'promtail'
        },
        name: 'promtail',
        namespace: namespace,
      },
      spec: {
        selector: {
          matchLabels: {
            app: 'promtail'
          }
        },
        template: {
          metadata: {
            labels: {
              app: 'promtail'
            },
            annotations: {
              'prometheus.io/port': 'http-metrics',
              'prometheus.io/scrape': 'true',
            },
          },
          spec: {
            containers: [
              {
                name: 'promtail',
                image: 'grafana/promtail:' + promtailImageTag,
                imagePullPolicy: promtailImagePullPolicy,
                args: [
                  '-config.file=/etc/promtail/promtail.yaml',
                  '-client.url=http://loki:3100/api/prom/push'
                ],
                env: [
                  {
                    name: 'HOSTNAME',
                    valueFrom: {
                      fieldRef: {
                        fieldPath: 'spec.nodeName'
                      }
                    }
                  }
                ],
                ports: [
                  {
                    containerPort: 3101,
                    name: 'http-metrics'
                  }
                ],
                readinessProbe: {
                  failureThreshold: 5,
                  httpGet: {
                    path: '/ready',
                    port: 'http-metrics'
                  },
                  initialDelaySeconds: 10,
                  periodSeconds: 10,
                  successThreshold: 1,
                  timeoutSeconds: 1
                },
                resources: {
                  limits: {
                    cpu: '250m',
                    memory: '180Mi'
                  },
                  requests: {
                    cpu: '50m',
                    memory: '60Mi'
                  }
                },
                securityContext: {
                  readOnlyRootFilesystem: true,
                  runAsGroup: 0,
                  runAsUser: 0
                },
                volumeMounts: [
                  {
                    mountPath: '/etc/promtail',
                    name: 'config'
                  },
                  {
                    mountPath: '/run/promtail',
                    name: 'run'
                  },
                  {
                    mountPath: '/var/lib/docker/containers',
                    name: 'docker',
                    readOnly: true
                  },
                  {
                    mountPath: '/var/log/pods',
                    name: 'pods',
                    readOnly: true
                  }
                ]
              }
            ],
            nodeSelector: {},
            serviceAccountName: 'promtail',
            tolerations: [
              {
                effect: 'NoSchedule',
                key: 'node-role.kubernetes.io/master'
              }
            ],
            volumes: [
              {
                configMap: {
                  name: 'promtail'
                },
                name: 'config'
              },
              {
                hostPath: {
                  path: '/run/promtail'
                },
                name: 'run'
              },
              {
                hostPath: {
                  path: '/var/lib/docker/containers'
                },
                name: 'docker'
              },
              {
                hostPath: {
                  path: '/var/log/pods'
                },
                name: 'pods'
              }
            ]
          }
        },
        updateStrategy: {
          type: 'RollingUpdate'
        }
      }
    },
    'promtail-configmap': {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      metadata: {
        labels: {
          app: 'promtail'
        },
        name: 'promtail',
        namespace: namespace,
      },
      data: {
        'promtail.yaml': 'client:\n  backoff_config:\n    maxbackoff: 5s\n    maxretries: 5\n    minbackoff: 100ms\n  batchsize: 102400\n  batchwait: 1s\n  external_labels: {}\n  timeout: 10s\npositions:\n  filename: /run/promtail/positions.yaml\nserver:\n  http_listen_port: 3101\ntarget_config:\n  sync_period: 10s\n\nscrape_configs:\n- job_name: kubernetes-pods-name\n  pipeline_stages:\n    - docker: {}\n    \n  kubernetes_sd_configs:\n  - role: pod\n  relabel_configs:\n  - source_labels:\n    - __meta_kubernetes_pod_label_name\n    target_label: __service__\n  - source_labels:\n    - __meta_kubernetes_pod_node_name\n    target_label: __host__\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __service__\n  - action: replace\n    replacement: $1\n    separator: /\n    source_labels:\n    - __meta_kubernetes_namespace\n    - __service__\n    target_label: job\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_namespace\n    target_label: namespace\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_name\n    target_label: instance\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_container_name\n    target_label: container_name\n  - action: labelmap\n    regex: __meta_kubernetes_pod_label_(.+)\n  - replacement: /var/log/pods/*$1/*.log\n    separator: /\n    source_labels:\n    - __meta_kubernetes_pod_uid\n    - __meta_kubernetes_pod_container_name\n    target_label: __path__\n- job_name: kubernetes-pods-app\n  pipeline_stages:\n    - docker: {}\n    \n  kubernetes_sd_configs:\n  - role: pod\n  relabel_configs:\n  - action: drop\n    regex: .+\n    source_labels:\n    - __meta_kubernetes_pod_label_name\n  - source_labels:\n    - __meta_kubernetes_pod_label_app\n    target_label: __service__\n  - source_labels:\n    - __meta_kubernetes_pod_node_name\n    target_label: __host__\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __service__\n  - action: replace\n    replacement: $1\n    separator: /\n    source_labels:\n    - __meta_kubernetes_namespace\n    - __service__\n    target_label: job\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_namespace\n    target_label: namespace\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_name\n    target_label: instance\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_container_name\n    target_label: container_name\n  - action: labelmap\n    regex: __meta_kubernetes_pod_label_(.+)\n  - replacement: /var/log/pods/*$1/*.log\n    separator: /\n    source_labels:\n    - __meta_kubernetes_pod_uid\n    - __meta_kubernetes_pod_container_name\n    target_label: __path__\n- job_name: kubernetes-pods-direct-controllers\n  pipeline_stages:\n    - docker: {}\n    \n  kubernetes_sd_configs:\n  - role: pod\n  relabel_configs:\n  - action: drop\n    regex: .+\n    separator: \'\'\n    source_labels:\n    - __meta_kubernetes_pod_label_name\n    - __meta_kubernetes_pod_label_app\n  - action: drop\n    regex: ^([0-9a-z-.]+)(-[0-9a-f]{8,10})$\n    source_labels:\n    - __meta_kubernetes_pod_controller_name\n  - source_labels:\n    - __meta_kubernetes_pod_controller_name\n    target_label: __service__\n  - source_labels:\n    - __meta_kubernetes_pod_node_name\n    target_label: __host__\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __service__\n  - action: replace\n    replacement: $1\n    separator: /\n    source_labels:\n    - __meta_kubernetes_namespace\n    - __service__\n    target_label: job\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_namespace\n    target_label: namespace\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_name\n    target_label: instance\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_container_name\n    target_label: container_name\n  - action: labelmap\n    regex: __meta_kubernetes_pod_label_(.+)\n  - replacement: /var/log/pods/*$1/*.log\n    separator: /\n    source_labels:\n    - __meta_kubernetes_pod_uid\n    - __meta_kubernetes_pod_container_name\n    target_label: __path__\n- job_name: kubernetes-pods-indirect-controller\n  pipeline_stages:\n    - docker: {}\n    \n  kubernetes_sd_configs:\n  - role: pod\n  relabel_configs:\n  - action: drop\n    regex: .+\n    separator: \'\'\n    source_labels:\n    - __meta_kubernetes_pod_label_name\n    - __meta_kubernetes_pod_label_app\n  - action: keep\n    regex: ^([0-9a-z-.]+)(-[0-9a-f]{8,10})$\n    source_labels:\n    - __meta_kubernetes_pod_controller_name\n  - action: replace\n    regex: ^([0-9a-z-.]+)(-[0-9a-f]{8,10})$\n    source_labels:\n    - __meta_kubernetes_pod_controller_name\n    target_label: __service__\n  - source_labels:\n    - __meta_kubernetes_pod_node_name\n    target_label: __host__\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __service__\n  - action: replace\n    replacement: $1\n    separator: /\n    source_labels:\n    - __meta_kubernetes_namespace\n    - __service__\n    target_label: job\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_namespace\n    target_label: namespace\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_name\n    target_label: instance\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_container_name\n    target_label: container_name\n  - action: labelmap\n    regex: __meta_kubernetes_pod_label_(.+)\n  - replacement: /var/log/pods/*$1/*.log\n    separator: /\n    source_labels:\n    - __meta_kubernetes_pod_uid\n    - __meta_kubernetes_pod_container_name\n    target_label: __path__\n- job_name: kubernetes-pods-static\n  pipeline_stages:\n    - docker: {}\n    \n  kubernetes_sd_configs:\n  - role: pod\n  relabel_configs:\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __meta_kubernetes_pod_annotation_kubernetes_io_config_mirror\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_label_component\n    target_label: __service__\n  - source_labels:\n    - __meta_kubernetes_pod_node_name\n    target_label: __host__\n  - action: drop\n    regex: ^$\n    source_labels:\n    - __service__\n  - action: replace\n    replacement: $1\n    separator: /\n    source_labels:\n    - __meta_kubernetes_namespace\n    - __service__\n    target_label: job\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_namespace\n    target_label: namespace\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_name\n    target_label: instance\n  - action: replace\n    source_labels:\n    - __meta_kubernetes_pod_container_name\n    target_label: container_name\n  - action: labelmap\n    regex: __meta_kubernetes_pod_label_(.+)\n  - replacement: /var/log/pods/*$1/*.log\n    separator: /\n    source_labels:\n    - __meta_kubernetes_pod_annotation_kubernetes_io_config_mirror\n    - __meta_kubernetes_pod_container_name\n    target_label: __path__',
      },
    },
    'loki-serviceaccount': {
      apiVersion: 'v1',
      kind: 'ServiceAccount',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      }
    },
    'loki-role': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'Role',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      },
      rules: [
        {
          apiGroups: [
            'extensions'
          ],
          resourceNames: [
            'loki'
          ],
          resources: [
            'podsecuritypolicies'
          ],
          verbs: [
            'use'
          ]
        }
      ]
    },
    'loki-rolebinding': {
      apiVersion: 'rbac.authorization.k8s.io/v1',
      kind: 'RoleBinding',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      },
      roleRef: {
        apiGroup: 'rbac.authorization.k8s.io',
        kind: 'Role',
        name: 'loki'
      },
      subjects: [
        {
          kind: 'ServiceAccount',
          name: 'loki'
        }
      ]
    },
    'loki-podsecuritypolicy': {
      apiVersion: 'policy/v1beta1',
      kind: 'PodSecurityPolicy',
      metadata: {
        labels: {
          app: 'loki',
        },
        name: 'loki',
        namespace: namespace,
      },
      spec: {
        allowPrivilegeEscalation: false,
        fsGroup: {
          ranges: [
            {
              max: 65535,
              min: 1
            }
          ],
          rule: 'MustRunAs'
        },
        hostIPC: false,
        hostNetwork: false,
        hostPID: false,
        privileged: false,
        readOnlyRootFilesystem: true,
        requiredDropCapabilities: [
          'ALL'
        ],
        runAsUser: {
          rule: 'MustRunAsNonRoot'
        },
        seLinux: {
          rule: 'RunAsAny'
        },
        supplementalGroups: {
          ranges: [
            {
              max: 65535,
              min: 1
            }
          ],
          rule: 'MustRunAs'
        },
        volumes: [
          'configMap',
          'emptyDir',
          'persistentVolumeClaim',
          'secret'
        ]
      }
    },
    'loki-statefulset': {
      apiVersion: 'apps/v1',
      kind: 'StatefulSet',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      },
      spec: {
        replicas: 1,
        selector: {
          matchLabels: {
            app: 'loki'
          }
        },
        serviceName: 'loki',
        template: {
          metadata: {
            labels: {
              app: 'loki',
              name: 'loki'
            },
            annotations: {
              'prometheus.io/port': 'http-metrics',
              'prometheus.io/scrape': 'true',
            },
          },
          spec: {
            securityContext: {
              fsGroup: 10001,
              runAsGroup: 10001,
              runAsNonRoot: true,
              runAsUser: 10001,
            },
            containers: [
              {
                name: 'loki',
                image: 'grafana/loki:' + lokiImageTag,
                imagePullPolicy: lokiImagePullPolicy,
                args: [
                  '-config.file=/etc/loki/loki.yaml'
                ],
                env: [
                  {
                    name: 'JAEGER_AGENT_HOST',
                    value: ''
                  }
                ],
                ports: [
                  {
                    containerPort: 3100,
                    name: 'http-metrics',
                    protocol: 'TCP'
                  }
                ],
                livenessProbe: {
                  httpGet: {
                    path: '/ready',
                    port: 'http-metrics'
                  },
                  initialDelaySeconds: 45
                },
                readinessProbe: {
                  httpGet: {
                    path: '/ready',
                    port: 'http-metrics'
                  },
                  initialDelaySeconds: 45
                },
                resources: {
                  limits: {
                    cpu: '500m',
                    memory: '256Mi'
                  },
                  requests: {
                    cpu: '50m',
                    memory: '75Mi'
                  }
                },
                securityContext: {
                  readOnlyRootFilesystem: true
                },
                volumeMounts: [
                  {
                    name: 'config',
                    mountPath: '/etc/loki',
                  },
                  {
                    name: 'storage',
                    mountPath: '/data',
                  }
                ]
              }
            ],
            serviceAccountName: 'loki',
            terminationGracePeriodSeconds: 30,
            tolerations: [],
            volumes: [
              {
                name: 'config',
                secret: {
                  secretName: 'loki',
                }
              }
            ] + (if lokiStorageSize == '' then [
              {
                name: 'storage',
                emptyDir: {},
              },
            ] else []),
          }
        },
       } + (
        if lokiStorageSize == '' then {} else {
          volumeClaimTemplates: [
            {
              metadata: {
                name: 'storage'
              },
              spec: {
                accessModes: [
                  'ReadWriteOnce'
                ],
                resources: {
                  requests: {
                    storage: lokiStorageSize,
                  },
                },
              },
            },
          ],
        }
      ),
    },
    'loki-service':   {
      apiVersion: 'v1',
      kind: 'Service',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      },
      spec: {
        ports: [
          {
            name: 'http-metrics',
            port: 3100,
            targetPort: 'http-metrics'
          }
        ],
        selector: {
          app: 'loki'
        }
      }
    },
    'loki-secret': {
      apiVersion: 'v1',
      kind: 'Secret',
      metadata: {
        labels: {
          app: 'loki'
        },
        name: 'loki',
        namespace: namespace,
      },
      stringData: {
        'loki.yaml': 'auth_enabled: false\nchunk_store_config:\n  max_look_back_period: 0\ningester:\n  chunk_block_size: 262144\n  chunk_idle_period: 15m\n  lifecycler:\n    ring:\n      kvstore:\n        store: inmemory\n      replication_factor: 1\nlimits_config:\n  enforce_metric_name: false\nschema_config:\n  configs:\n  - from: "2018-04-15"\n    index:\n      period: 168h\n      prefix: index_\n    object_store: filesystem\n    schema: v9\n    store: boltdb\nserver:\n  http_listen_port: 3100\nstorage_config:\n  boltdb:\n    directory: /data/loki/index\n  filesystem:\n    directory: /data/loki/chunks\ntable_manager:\n  retention_deletes_enabled: false\n  retention_period: 0',
      },
    },
  },
}
