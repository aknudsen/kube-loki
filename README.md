# Kube-Loki
This is a Jsonnet library for deploying the Loki logging back-end to Kubernetes.
Include it in your own Jsonnet project via
[Jsonnet-Bundler](https://github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb).

An example of its usage is in example.jsonnet, use this to generate manifests in the manifests/
directory by running `./build.sh`.

## Prerequisites
* [Jsonnet](https://jsonnet.org/)
* [Jsonnet-Bundler](https://github.com/jsonnet-bundler/jsonnet-bundler)
* [gojsontoyaml](https://github.com/brancz/gojsontoyaml)
